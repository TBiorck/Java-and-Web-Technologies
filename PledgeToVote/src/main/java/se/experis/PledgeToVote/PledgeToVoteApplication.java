package se.experis.PledgeToVote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PledgeToVoteApplication {

	public static void main(String[] args) {
		SpringApplication.run(PledgeToVoteApplication.class, args);
	}

}
