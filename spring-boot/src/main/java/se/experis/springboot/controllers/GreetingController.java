package se.experis.springboot.controllers;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class GreetingController {

    @RequestMapping(value = "/greeting", method = GET)
    public String greetingWithInput(@RequestParam(value = "name", defaultValue = "stranger") String name) {
        return "Hello, " + name + "!";
    }

    @RequestMapping(value = "/greeting-reverse", method = GET)
    public String reverseInput(@RequestParam(value = "name", defaultValue = "stranger") String name) {
        return reverseString(name);
    }

    private String reverseString(String inputString) {
        return new StringBuilder(inputString).reverse().toString();
    }

}
