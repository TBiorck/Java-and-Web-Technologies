/*
    The functions declared in this file are referenced by Index.html.
 */

// The APIs including param "name"
const GREETING_API = "/greeting?name="
const GREETING_REVERSE_API = "/greeting-reverse?name="

/**
 * Append the value currently in the input field of the Index.html and fetch the greeting API.
 * @returns {Promise<void>}
 */
async function greeting() {
    let inputText = document.getElementById("nameInput").value

    let response = await fetch(GREETING_API + inputText)

    let text = await response.text()

    setInnerTextOfElement(text, document.getElementById("greeting-text"))
}

/**
 * Append the value currently in the input field of the Index.html and fetch the greeting-reverse API.
 * @returns {Promise<void>}
 */
async function reverse() {
    let inputText = document.getElementById("nameInput").value

    let response = await fetch(GREETING_REVERSE_API + inputText)

    let text = await response.text()

    setInnerTextOfElement(text, document.getElementById("reverse-text"))
}

/**
 * Set the inner text of an HTML Element.
 * @param {string} text
 * @param element An HTML element.
 */
function setInnerTextOfElement(text, element) {
    element.innerText = text
}