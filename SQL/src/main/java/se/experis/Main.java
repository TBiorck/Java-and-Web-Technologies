package se.experis;

import se.experis.database.DatabaseHandler;
import se.experis.entity.Customer;
import se.experis.view.Console;

public class Main {
    public static void main(String[] args) {
        Console console = new Console();
        DatabaseHandler dbHandler = new DatabaseHandler();

        /* FIND THE CUSTOMER BASED ON THE ENTERED ID */
        String customerID = console.askForCustomerID();
        Customer customer = dbHandler.getCustomer(customerID);
        console.displayCustomerInfo(customer.getCustomerID(), customer.getFirstName(), customer.getLastName());


        /* FIND THE CUSTOMER'S TOP GENRE */
        String genre = dbHandler.getMostPopularGenre(customer);
        console.displayMostPopularGenre(genre);


        /* FIND A RANDOM CUSTOMER BY ENTERING EMPTY ID STRING */
        Customer randomCustomer = dbHandler.getCustomer("");
        console.displayCustomerInfo(randomCustomer.getCustomerID(), randomCustomer.getFirstName(), randomCustomer.getLastName());
        console.displayMostPopularGenre(dbHandler.getMostPopularGenre(randomCustomer));
    }
}
