package se.experis.database;

import se.experis.entity.Customer;

import java.sql.*;

public class DatabaseHandler {
    private final String CONNECTION_STRING = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;


    //Selects the most frequent genre of the input customer.
    public String getMostPopularGenre(Customer customer) {
        try {
            conn = getNewConnection();

            PreparedStatement preparedStatement = conn.prepareStatement("" +
                    "SELECT Name " +
                    "FROM Genre " +
                    "WHERE GenreId =" +
                    "(SELECT GenreId " +
                    "FROM" +
                    "(SELECT GenreId " +
                    "FROM Track " +
                    "WHERE TrackId IN " +
                    "(SELECT TrackId " +
                    "FROM InvoiceLine " +
                    "WHERE InvoiceId IN " +
                    "(SELECT InvoiceId " +
                    "FROM Invoice " +
                    "WHERE CustomerId = ?)))" +
                    "GROUP BY GenreId " +
                    "ORDER BY COUNT(GenreId) DESC " +
                    "LIMIT 1)"
            );
            preparedStatement.setString(1, customer.getCustomerID());

            ResultSet resultSet = preparedStatement.executeQuery();

            return resultSet.getString("Name");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            closeConnection();
        }
        return null;
    }

    /* Get a customer from the database based on ID. If input is incorrect (null or not numbers),
       a random customer is fetched.
     */
    public Customer getCustomer(String customerID) {
        try {
            if (!customerID.matches("[0-9]+") || customerID == null) {
                customerID = getRandomCustomerID();
                System.out.println("NOTICE: Random customer ID was generated because of none, or faulty input.");
            }

            conn = getNewConnection();
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT FirstName, LastName FROM Customer WHERE CustomerId = ?");
            preparedStatement.setString(1, customerID);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.isClosed()) {
                System.out.println("Error: Found no customer with that ID.");
                return null;
            }

            Customer customer = new Customer(customerID, resultSet.getString("FirstName"), resultSet.getString("LastName"));

            return customer;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            closeConnection();
        }
        return null;
    }

    private String getRandomCustomerID() {
        try {
            conn = getNewConnection();
            PreparedStatement preparedStatement = conn.prepareStatement("" +
                            "SELECT CustomerId " +
                            "FROM Customer " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 1");

            ResultSet resultSet = preparedStatement.executeQuery();

            return resultSet.getString("CustomerId");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            closeConnection();
        }
        return null;
    }

    private Connection getNewConnection() {
        try {
            Connection conn = DriverManager.getConnection(CONNECTION_STRING);
//            System.out.println("Connection to SQLite has been established.");
            return conn;
        }
        catch (SQLException e) {
            System.out.println("Error: Could not open a connection to SQLite.");
        }
        return null;
    }

    private void closeConnection() {
        try {
            conn.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
