package se.experis.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {

    public String askForCustomerID() {
        System.out.println("\nEnter a customer ID: ");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            return reader.readLine();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void displayMostPopularGenre(String genre) {
        System.out.println("The entered customer's most popular genre is: " + genre);
    }

    public void displayCustomerInfo(String customerID, String firstName, String lastName) {
        System.out.println("Customer: " + firstName + " " + lastName + " (ID: " + customerID + ")");
    }
}
